import 'package:paquete_mongo/paquete_mongo.dart';
import 'package:ohanami/repositorio_local/repositorio_local.dart';
import 'componentes.dart';
import 'package:flutter/material.dart';
class VistaIniciarSesion extends StatelessWidget {
  const VistaIniciarSesion({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    TextEditingController nombre = TextEditingController();
    TextEditingController clave = TextEditingController();
    RepositorioLocal local = RepositorioLocal();
    RepositorioMongo mongo = RepositorioMongo();
    
     mostrarMensaje(String mensaje){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar( content: Text(mensaje)));
    }

    void iniciarSesion() async{
      
      bool mongoInicializadoConExito = await mongo.inicializar();
      if (mongoInicializadoConExito == true) {
        Usuario usuario = Usuario(nombre: nombre.text.toString() ,clave: clave.text.toString() , partidas: [], correo: "");
        bool sesionIniciadaConExito = await mongo.verificarInicioSesion(usuario: usuario);
        if (sesionIniciadaConExito == true) {
          Usuario usuarioMongo = await  mongo.recuperarUsuario(usuario: usuario);
          local.registrarUsuario(usuario: usuarioMongo);
          Navigator.push( context, MaterialPageRoute(builder: (context) => VistaListaPartidas()));
        }
        else
        {   
        mostrarMensaje("Datos de inicio de sesion incorrectos");
        }
      }
      else
      {
        mostrarMensaje("No tienes conexion a internet, prueba jugando local");

      }
    }
   
    void registrarmeDespues() async{
      Usuario usuario =Usuario(nombre: "", correo: "", clave: clave.text, partidas: []);
      local.registrarUsuario(usuario: usuario);
    }

    widgetUsername() {
        return  Container(
          child: TextField(
            controller: nombre,
            keyboardType: TextInputType.name,
            decoration: InputDecoration(
              icon: const Icon(Icons.person),
              hintText: 'Nombre de usuario',
              labelText: 'Nombre de usuario',
            ),
          ),
        );
    }

    widgetPassword() {
        return  Container(
          child: TextField(
            obscureText:true,
            controller: clave,
            keyboardType: TextInputType.visiblePassword,
            decoration: InputDecoration(
              icon: const Icon(Icons.password),
              hintText: 'Contraseña',
              labelText: 'Contraseña',
            ),
          ),
        );
    }

    widgetBotonIniciarSesion(){
      return StreamBuilder(
                builder: (BuildContext context, AsyncSnapshot snapshot){
                  return ElevatedButton(
                    onPressed: () async{
                      
                      iniciarSesion();
                    },
                    child: Container(
                      padding: EdgeInsets.all(15.0),
                      child: Text('Iniciar Sesion',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                      ),
                    ),
                    style:ElevatedButton.styleFrom(
                      elevation: 10.0, 
                      textStyle: const TextStyle(
                        fontSize: 20,
                        
                        )
                        ),
                    );
                }
              );
    }

    widgetBotonRegistrarmeDespues(){
      return StreamBuilder(
        builder: (BuildContext context, AsyncSnapshot snapshot){
          return ElevatedButton(
            onPressed: () async {
              registrarmeDespues();
              Navigator.push(context,
              MaterialPageRoute(builder: (context) => VistaListaPartidas()));
            },
            child: Container(
              padding: EdgeInsets.all(15.0),
              child: Text('Sin conexion',
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
              ),
              ),
            ),
            style:ElevatedButton.styleFrom(
              elevation: 10.0, 
              textStyle: const TextStyle(
                fontSize: 20,
                
                )
                ),
            );
        }
      );
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(5, 35, 5, 15),
              child: Image.asset('assets/images/icono.jpeg',
              height: 200,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: widgetUsername(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: widgetPassword()
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: widgetBotonIniciarSesion()
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: widgetBotonRegistrarmeDespues(),
            ),
          ],
        ),
      ),
    );


  }

  
}

