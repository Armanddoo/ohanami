
import 'package:flutter/material.dart';
import 'package:partida/partida.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'lista_partida.dart';
class DetallePartida extends StatefulWidget {
  const DetallePartida({Key? key, required this.partida}) : super(key: key);
  final Partida partida;

  @override
  _DetallePartidaState createState() => _DetallePartidaState(partida);
}

class _DetallePartidaState extends State<DetallePartida> {
  Partida partida;

  List<charts.Series<CRonda1, String>> _seriesData1 = [];
  List<charts.Series<CRonda2, String>> _seriesData2 = [];
  List<charts.Series<CRonda3, String>> _seriesData3 = [];
  List<charts.Series<PuntuacionJugador, String>> _seriesData4 = [];
  
  List<PuntuacionJugador> puntuacionesronda1 = [];
  List<PuntuacionJugador> puntuacionesronda2 = [];
  List<PuntuacionJugador> puntuacionesronda3 = [];
  List<PuntuacionJugador> puntuacionesrondaDesenlace = [];

  List<CRonda1> cartasRonda1 = [];
  List<CRonda2> cartasRonda2 = [];
  List<CRonda3> cartasRonda3 = [];

      var nombreJugadorGanador = "";
    var puntuacionJugadoreGanador =0;

  _DetallePartidaState(this.partida);


  @override
  void initState(){
    super.initState();
    calcularPuntuacionesPartida();
    _generarDataR1();
    _generarDataR2();
    _generarDataR3();
  }
  void _buscarGanador(){

    for (var jugador in puntuacionesrondaDesenlace) {
        var puntuacionJugadorActual =jugador.porAzules+jugador.porRosas+jugador.porVerdes+jugador.porNegras;
        if(puntuacionJugadorActual>puntuacionJugadoreGanador){
            nombreJugadorGanador=jugador.jugador.nombre;
            puntuacionJugadoreGanador=puntuacionJugadorActual;
        }
    }
  }



  void calcularPuntuacionesPartida(){
    puntuacionesronda1 = partida.puntos(ronda: FasePuntuacion.Ronda1);
    puntuacionesronda2 = partida.puntos(ronda: FasePuntuacion.Ronda2);
    puntuacionesronda3 = partida.puntos(ronda: FasePuntuacion.Ronda3);
    puntuacionesrondaDesenlace = partida.puntos(ronda: FasePuntuacion.desenlace);
  }

  _generarDataR1() {
    _seriesData1.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasAzules ,
        id: '1',
        data: partida.puntuacionesRonda1,
        seriesCategory: '1',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (CRonda1 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF337CFF)),
      ), 
    );
  }
  _generarDataR2() {
    _seriesData2.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasAzules ,
        id: '2',
        data: partida.puntuacionesRonda2,
        seriesCategory: '21',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (CRonda2 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF337CFF)),
      ), 
    );
    _seriesData2.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasVerdes ,
        id: '2',
        data: partida.puntuacionesRonda2,
        seriesCategory: '22',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (CRonda2 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF00FF00)),
      ), 
    );
  }
  _generarDataR3() {
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasAzules,
        id: '3',
        data: partida.puntuacionesRonda3,
        seriesCategory: '31',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (CRonda3 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF337CFF)),
      ), 
    );
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasVerdes,
        id: '3',
        data: partida.puntuacionesRonda3,
        seriesCategory: '32',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (CRonda3 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF00FF00)),
      ), 
    );
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasRosas,
        id: '3',
        data: partida.puntuacionesRonda3,
        seriesCategory: '33',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (CRonda3 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFFFF66FF)),
      ), 
    );
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.cuantasNegras,
        id: '3',
        data: partida.puntuacionesRonda3,
        seriesCategory: '34',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (CRonda3 partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF555555)),
      ), 
    );
  }
  
 
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 3,
      child: Scaffold(
      appBar: AppBar(
        leading: IconButton(onPressed: (){
          Navigator.push( context, MaterialPageRoute(builder: (context) => VistaListaPartidas()));
        }, 
        icon: Icon(Icons.arrow_back)),
          title: const Text('Informacion por Rondas'),
          bottom: const TabBar(
            tabs: <Widget>[
              Tab(
                child: Text("Primera"),
                icon: Icon(Icons.looks_one),
              ),
              Tab(
                child: Text("Segunda"),
                icon: Icon(Icons.looks_two),
              ),
              Tab(
                child: Text("Tercera"),
                icon: Icon(Icons.looks_3),
              ),
  
            ],
          ),
      ),
      body:TabBarView(
            children: <Widget>[
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text("Cartas jugadas en la ronda 1", style: TextStyle(fontSize: 20),),
                  ),
                  Expanded(
                    child: _graficaRonda1(),
                  ),
                ],
              ),
               Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text("Cartas jugadas en la ronda 2", style: TextStyle(fontSize: 20),),
                  ),
                  Expanded(
                    child: _graficaRonda2(),
                  ),
                ],
              ),
               Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text("Cartas jugadas en la ronda 3", style: TextStyle(fontSize: 20),),
                  ),
                  Expanded(
                    child: _graficaRonda3(),
                  ),
                ],
              ),         
            ],
          ),
      ),
    );
  }
  
 _graficaRonda3(){
  return Container(
    child: charts.BarChart(
      _seriesData3,
      animate: true,
      barGroupingType: charts.BarGroupingType.groupedStacked,
      animationDuration: Duration(seconds: 1),
    ),
  );
}
 _graficaRonda2(){
  return Container(
    child: charts.BarChart(
      _seriesData2,
      animate: true,
      barGroupingType: charts.BarGroupingType.groupedStacked,
      animationDuration: Duration(seconds: 1),
    ),
  );
}
 _graficaRonda1(){
  return Container(
    child: charts.BarChart(
      _seriesData1,
      animate: true,
      barGroupingType: charts.BarGroupingType.groupedStacked,
      animationDuration: Duration(seconds: 1),
    ),
  );
}
}
