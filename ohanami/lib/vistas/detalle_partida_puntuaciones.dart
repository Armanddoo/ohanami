
import 'package:flutter/material.dart';
import 'package:partida/partida.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'lista_partida.dart';
class DetallePartidaPuntuaciones extends StatefulWidget {
  const DetallePartidaPuntuaciones({Key? key, required this.partida}) : super(key: key);
  final Partida partida;

  @override
  _DetallePartidaPuntuacionesState createState() => _DetallePartidaPuntuacionesState(partida);
}

class _DetallePartidaPuntuacionesState extends State<DetallePartidaPuntuaciones> {
  Partida partida;

  List<charts.Series<PuntuacionJugador, String>> _seriesData1 = [];
  List<charts.Series<PuntuacionJugador, String>> _seriesData2 = [];
  List<charts.Series<PuntuacionJugador, String>> _seriesData3 = [];
  List<charts.Series<PuntuacionJugador, String>> _seriesData4 = [];
  
  List<PuntuacionJugador> puntuacionesronda1 = [];
  List<PuntuacionJugador> puntuacionesronda2 = [];
  List<PuntuacionJugador> puntuacionesronda3 = [];
  List<PuntuacionJugador> puntuacionesrondaDesenlace = [];

  Map<String,int> puntuaciones ={};


  List<CRonda1> cartasRonda1 = [];
  List<CRonda2> cartasRonda2 = [];
  List<CRonda3> cartasRonda3 = [];

      var nombreGanadorFinal = "";
    var puntuacionesGanadorFinal =0;
    var nombreGanadorR1= "";
    var puntuacionesGanadorR1 =0;

    var nombreGanadorR2 = "";
    var puntuacionesGanadorR2 =0;

    var nombreGanadorR3 = "";
    var puntuacionesGanadorR3 =0;

  _DetallePartidaPuntuacionesState(this.partida);


  @override
  void initState(){
    super.initState();
    calcularPuntuacionesPartida();
    _generarDataR1();
    _generarDataR2();
    _generarDataR3();
    _generarDataR4();
     _buscarGanadorR1();
    _buscarGanadorR2();
    _buscarGanadorR3();
    _buscarGanador();

  }

  void _buscarGanador(){

    for (var jugador in puntuacionesrondaDesenlace) {
        var puntuacionJugadorActual =jugador.porAzules+jugador.porRosas+jugador.porVerdes+jugador.porNegras;
        puntuaciones.addAll({
         jugador.jugador.nombre : puntuacionJugadorActual
        });
        if(puntuacionJugadorActual>puntuacionesGanadorFinal){
            nombreGanadorFinal=jugador.jugador.nombre;
            puntuacionesGanadorFinal=puntuacionJugadorActual;
        }
    }
  }

  void _buscarGanadorR1(){

    for (var jugador in puntuacionesronda1) {
        var puntuacionJugadorActual =jugador.porAzules+jugador.porRosas+jugador.porVerdes+jugador.porNegras;
        puntuaciones.addAll({
         jugador.jugador.nombre : puntuacionJugadorActual
        });
        if(puntuacionJugadorActual>puntuacionesGanadorR1){
            nombreGanadorR1=jugador.jugador.nombre;
            puntuacionesGanadorR1=puntuacionJugadorActual;
        }
    }
  }

  void _buscarGanadorR2(){

    for (var jugador in puntuacionesronda2) {
        var puntuacionJugadorActual =jugador.porAzules+jugador.porRosas+jugador.porVerdes+jugador.porNegras;
        puntuaciones.addAll({
         jugador.jugador.nombre : puntuacionJugadorActual
        });
        if(puntuacionJugadorActual>puntuacionesGanadorR2){
            nombreGanadorR2=jugador.jugador.nombre;
            puntuacionesGanadorR2=puntuacionJugadorActual;
        }
    }
  }
  void _buscarGanadorR3(){

    for (var jugador in puntuacionesronda3) {
        var puntuacionJugadorActual =jugador.porAzules+jugador.porRosas+jugador.porVerdes+jugador.porNegras;
        puntuaciones.addAll({
         jugador.jugador.nombre : puntuacionJugadorActual
        });
        if(puntuacionJugadorActual>puntuacionesGanadorR3){
            nombreGanadorR3=jugador.jugador.nombre;
            puntuacionesGanadorR3=puntuacionJugadorActual;
        }
    }
  }

  void calcularPuntuacionesPartida(){
    puntuacionesronda1 = partida.puntos(ronda: FasePuntuacion.Ronda1);
    puntuacionesronda2 = partida.puntos(ronda: FasePuntuacion.Ronda2);
    puntuacionesronda3 = partida.puntos(ronda: FasePuntuacion.Ronda3);
    puntuacionesrondaDesenlace = partida.puntos(ronda: FasePuntuacion.desenlace);
  }

  _generarDataR1() {
    _seriesData1.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porAzules ,
        id: '1',
        data: puntuacionesronda1,
        seriesCategory: '1',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF337CFF)),
      ), 
    );
  }
  _generarDataR2() {
    _seriesData2.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porAzules ,
        id: '2',
        data: puntuacionesronda2,
        seriesCategory: '21',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF337CFF)),
      ));
      _seriesData2.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porVerdes ,
        id: '2',
        data: puntuacionesronda2,
        seriesCategory: '22',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF00FF00)),
      ));
  }
  _generarDataR3() {
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porAzules,
        id: '3',
        data: puntuacionesronda3,
        seriesCategory: '31',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF337CFF)),
      ), 
    );
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porVerdes,
        id: '3',
        data: puntuacionesronda3,
        seriesCategory: '32',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF00FF00)),
      ), 
    );
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porRosas,
        id: '3',
        data: puntuacionesronda3,
        seriesCategory: '33',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFFFF66FF)),
      ), 
    );
    _seriesData3.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porNegras,
        id: '3',
        data: puntuacionesronda3,
        seriesCategory: '34',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF555555)),
      ), 
    );
  }
  _generarDataR4() {
    _seriesData4.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porAzules,
        id: '4',
        data: puntuacionesrondaDesenlace,
        seriesCategory: '41',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF337CFF)),
      ), 
    );
    _seriesData4.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porVerdes,
        id: '4',
        data: puntuacionesrondaDesenlace,
        seriesCategory: '42',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF00FF00)),
      ), 
    );
    _seriesData4.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porRosas,
        id: '4',
        data: puntuacionesrondaDesenlace,
        seriesCategory: '43',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFFFF66FF)),
      ), 
    );
    _seriesData4.add(
      charts.Series(
        domainFn: (partida, _) =>  partida.jugador.nombre.toString(),
        measureFn: (partida, _) =>partida.porNegras,
        id: '4',
        data: puntuacionesrondaDesenlace,
        seriesCategory: '44',
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (PuntuacionJugador partida, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF555555)),
      ), 
    );
  }
 
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 4,
      child: Scaffold(
      appBar: AppBar(
        leading: IconButton(onPressed: (){
          Navigator.push( context, MaterialPageRoute(builder: (context) => VistaListaPartidas()));
        }, 
        icon: Icon(Icons.arrow_back)),
          title: const Text('Puntuaciones por Rondas'),
          bottom: const TabBar(
            tabs: <Widget>[
              Tab(
                child: Text("Primera"),
                icon: Icon(Icons.looks_one),
              ),
              Tab(
                child: Text("Segunda"),
                icon: Icon(Icons.looks_two),
              ),
              Tab(
                child: Text("Tercera"),
                icon: Icon(Icons.looks_3),
              ),
              Tab(
                child: Text("Final"),
                icon: Icon(Icons.account_balance_outlined),
              ),
  
            ],
          ),
      ),
      body:TabBarView(
            children: <Widget>[
              Column(
                children: [
                  Text("Ganó el jugador $nombreGanadorR1 con $puntuacionesGanadorR1 puntos", style: TextStyle(fontSize: 20),),  
                  Expanded(
                    child: _graficaRonda1(),
                  ),
                ],
              ),
               Column(
                children: [
                  Text("Ganó el jugador $nombreGanadorR2 con $puntuacionesGanadorR2 puntos", style: TextStyle(fontSize: 20),),  
                  Expanded(
                    child: _graficaRonda2(),
                  ),
                ],
              ),
               Column(
                children: [
                  Text("Ganó el jugador $nombreGanadorR3 con $puntuacionesGanadorR3 puntos", style: TextStyle(fontSize: 20),),  
                  Expanded(
                    child: _graficaRonda3(),
                  ),
                ],
              ),
               Column(
                children: [
                  Text("Ganó el jugador $nombreGanadorFinal con $puntuacionesGanadorFinal puntos", style: TextStyle(fontSize: 20),),
                  Expanded(
                    child: _graficaPuntuacionesRondaFinal(),
                  ),
                ],
              ),
              
            ],
          ),
      ),
    );

  }

_graficaPuntuacionesRondaFinal(){
  return Container(
    child: charts.BarChart(
      _seriesData4,
      animate: true,
      barGroupingType: charts.BarGroupingType.groupedStacked,
      animationDuration: Duration(seconds: 1),
    ),
  );
}

 _graficaRonda3(){
  return Container(
    child: charts.BarChart(
      _seriesData3,
      animate: true,
      barGroupingType: charts.BarGroupingType.groupedStacked,
      animationDuration: Duration(seconds: 1),
    ),
  );
}
 _graficaRonda2(){
  return Container(
    child: charts.BarChart(
      _seriesData2,
      animate: true,
      barGroupingType: charts.BarGroupingType.groupedStacked,
      animationDuration: Duration(seconds: 1),
    ),
  );
}
 _graficaRonda1(){
  return Container(
    child: charts.BarChart(
      _seriesData1,
      animate: true,
      barGroupingType: charts.BarGroupingType.groupedStacked,
      animationDuration: Duration(seconds: 1),
    ),
  );
}
}
