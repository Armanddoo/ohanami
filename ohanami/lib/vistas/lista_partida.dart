import 'package:ohanami/repositorio_local/repositorio_local.dart';
import 'componentes.dart';
import 'package:flutter/material.dart';
import 'package:paquete_mongo/paquete_mongo.dart';
import 'package:partida/partida.dart';

class VistaListaPartidas extends StatefulWidget {
  const VistaListaPartidas({Key? key}) : super(key: key);

  @override
  VistaListaPartidasState createState() => VistaListaPartidasState();
}

class VistaListaPartidasState extends State<VistaListaPartidas> {
  RepositorioLocal local = RepositorioLocal();

  late Future<Usuario> usuario;
  @override
  void initState() {
    usuario = local.recuperarUsuarioLocal();
    super.initState();
  }

  void sincronizarDB() async {
    RepositorioLocal local = RepositorioLocal();
    RepositorioMongo mongo = RepositorioMongo();
    Usuario usuarioLocal = await local.recuperarUsuarioLocal();
    bool mongoInicializadoConExito = await mongo.inicializar();
    if (mongoInicializadoConExito == true) {
      bool usuarioRegistradoEnMongo = await mongo.registradoUsuario(usuario: usuarioLocal);
      if (usuarioRegistradoEnMongo == true) {
         await mongo.reescribirPartidas(usuario: usuarioLocal);
      } else {
         await mongo.registrarUsuario(usuario: usuarioLocal);
      }
    }
  }
  floatingActionPersonalizado(){
  return FloatingActionButton(
    child: Icon(Icons.add),
    onPressed: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => const VistaNuevaPartida()));
    });
  }
  
  drawerPersonalizado(){
    return FutureBuilder(
    future: usuario,
    builder: (BuildContext context, AsyncSnapshot<Usuario> snapshot) {
      return snapshot.hasData
          ? snapshot.hasError
              ? Text("Hubo un error recuperando los datos")
              :// 
              Drawer(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    children: [
                      DrawerHeader(
                        decoration: BoxDecoration(color: Colors.blue),
                        child: Text(
                              '',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 24,
                              ),
                            ),
                      ),
                      snapshot.data!.nombre.isNotEmpty
                          ? 
                          //Si esta logueado
                          Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.logout_outlined),
                                  title: Text("Cerrar Sesión"),
                                  onTap: () async {                           
                                    _showMyDialog();                                      
                                  },
                                ),
                              ],
                            )
                            //Si no está registrado
                          : ListTile(
                             leading: Icon(Icons.login_outlined),
                              title: Text("Registrarme"),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const VistaRegistrarJugador()));
                              },
                            ),
                          ],
                  ),
                )
           // cargando
          : CircularProgressIndicator();
     
    },
  );
  }
  
  listaDePartidas(){
   return  FutureBuilder<Usuario>(
                future: local.recuperarUsuarioLocal(),
                builder:
                    (BuildContext context, AsyncSnapshot<Usuario> snapshot) {
                  return snapshot.hasData
                      ?
                      // data SÍ, entonces
                      // preguntar si tiene error
                      snapshot.hasError
                          ?
                          // error sí
                          // vista error
                          Text("error al obtener datos")
                          :
                          // error no
                          // vista lista
                          snapshot.data!.partidas.isEmpty
                              ? Image.asset('assets/images/dogg.png')
                              : _listaDatos(snapshot)
                      :
                      // data NO, entonces
                      // vista cargando
                      const CircularProgressIndicator();
                });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          
        ),
        floatingActionButton: floatingActionPersonalizado(),
        drawer: drawerPersonalizado(), 
        body: Center(
            child: 
             Padding(
                  padding: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                  child:  listaDePartidas(),

      ),
    )));
  }

Future<void> _showMyDialog() async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('WARNING'),
        content: SingleChildScrollView(
          child: ListBody(
            children: const <Widget>[
              Text('Si cierra sesión y no tiene conexion no habrá un respaldo de datos'),
              Text('¡Esta seguro de cerrar sesión?'),
            ],
          ),
        ),
        actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancelar'),
              child: const Text('Cancelar'),
            ),
            TextButton(
              onPressed: () => {
               sincronizarDB(),
            local.limpiarInfoSP(),
                 Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    const VistaIniciarSesion()))

              },
              child: const Text('Cerrar Sesión'),
            ),
          ],
      );
    },
  );
}



  _listaDatos(snapshot) {
    return ListView.builder(
        itemCount: snapshot.data!.partidas.length,
        itemBuilder: (BuildContext context, int index) {
          int reverseIndex = snapshot.data!.partidas.length - 1 - index;
          print(reverseIndex); // verificador
          FasePuntuacion.Ronda1;
          FasePuntuacion.Ronda2;
          FasePuntuacion.Ronda3;
          snapshot.data!.partidas[0].puntos(ronda: FasePuntuacion.Ronda1);
          List<String> nombres = [];
          for (var i = 0;
              i < snapshot.data!.partidas[reverseIndex].jugadores.length;
              i++) {
            nombres.add(snapshot.data!.partidas[reverseIndex].jugadores
                .elementAt(i)
                .nombre
                .toString());
          }
          do {
            nombres.add("");
          } while (nombres.length < 4);
  
          return Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Image.asset('assets/images/hanami.png',
                  height: 300,),
                  title: const Text('Partida de ohanami',style: TextStyle(fontSize: 20,fontStyle: FontStyle.normal),),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top:15),
                        child: Text(
                          'Participantes:',
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      Text(nombres[0]),
                      Text(nombres[1]),
                      Text(nombres[2]),
                      Text(nombres[3]),
                    ],
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DetallePartidaPuntuaciones(
                                    partida: snapshot
                                        .data!.partidas[reverseIndex])));
                      },
                      child: const Text('Puntaje'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DetallePartida(
                                    partida: snapshot
                                        .data!.partidas[reverseIndex])));
                      },
                      child: const Text('Estrategia'),
                    ),
                    ElevatedButton(
                      onPressed: () async {

                        await local
                            .eliminarPartida(indice: reverseIndex)
                            .whenComplete(() {
                          setState(() {
                            snapshot.data!.partidas.removeAt(reverseIndex);
                          });
                        });
                      },
                      child: 
                        Icon(Icons.delete_forever)
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }
}