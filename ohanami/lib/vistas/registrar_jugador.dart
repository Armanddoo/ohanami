import 'dart:async';

import 'package:paquete_mongo/paquete_mongo.dart';
import 'package:ohanami/repositorio_local/repositorio_local.dart';
import 'package:ohanami/vistas/lista_partida.dart';
import 'package:flutter/material.dart';

class VistaRegistrarJugador extends StatefulWidget {
  const VistaRegistrarJugador({ Key? key,}) : super(key: key);
  @override
  State<VistaRegistrarJugador> createState() => _VistaRegistrarJugadorState();
}

class _VistaRegistrarJugadorState extends State<VistaRegistrarJugador> {
  bool check = false;
  TextEditingController nombre = TextEditingController();
  TextEditingController correo = TextEditingController();
  TextEditingController clave = TextEditingController();
  @override
  void initState() {
    super.initState();
  }

   Future validacion() async{
    RepositorioMongo mongo = RepositorioMongo();
    RepositorioLocal local = RepositorioLocal();
    local.limpiarInfoSP();
    
    bool mongocheck = await mongo.inicializar();
    bool checkv = await local.usuarioYaRegistrado();

    if (mongocheck == true && checkv == true) {
      Usuario usuario = await local.recuperarUsuarioLocal();
      checkv = await mongo.registradoUsuario(usuario: usuario);
      if (checkv == true) {
        Usuario u = await mongo.recuperarUsuario(usuario: usuario);
        checkv = await local.registrarUsuario(usuario: u);
        setState(() {
          check = checkv;
        });
      }
      setState(() {
        check = true;
      });
    }
    if (mongocheck == false && checkv == true) {
      setState(() {
        check = checkv;
      });
    }
  }

  void validarRegistro() async{
    RepositorioMongo mongo = RepositorioMongo();
    Usuario u = Usuario(nombre: nombre.text, correo: correo.text, clave: clave.text, partidas: []);
    bool nombreUsuarioEnUso = await mongo.registradoUsuario(usuario:u);
    if (nombreUsuarioEnUso == true) {
      mostrarMensaje("Este nombre de usuario ya esta registrado");
    }
    else
    {
      RepositorioLocal local = RepositorioLocal();
      bool usuarioLocalActualizado = await local.actualizarDatosUsuario(usuarioNuevo:u);
      if (usuarioLocalActualizado == true) {
        Usuario usuarioLocal= await  local.recuperarUsuarioLocal();
        mostrarMensaje("Usuario actualizado en local");
        RepositorioMongo mongo=RepositorioMongo();

        bool usuarioRegistradoConExito= await mongo.registrarUsuario(usuario: usuarioLocal);
            usuarioRegistradoConExito == true ? mostrarMensaje("usuario registrado en mongo") : mostrarMensaje("hubo un error al registrar");
      Navigator.push( context, MaterialPageRoute(builder: (context) => VistaListaPartidas()));
      }
    }
  }  


mostrarMensaje(String mensaje){
  ScaffoldMessenger.of(context).showSnackBar(SnackBar( content: Text(mensaje)));
}

widgetUsername() {
  return  Container(
    child: TextField(
      controller: nombre,
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
        icon: const Icon(Icons.person),
        hintText: 'Nombre de usuario',
        labelText: 'Nombre de usuario',
      ),
    ),
  );
}

    widgetPassword() {
        return  Container(
          child: TextField(
            obscureText:true,
            controller: clave,
            keyboardType: TextInputType.visiblePassword,
            decoration: InputDecoration(
              icon: const Icon(Icons.password),
              hintText: 'Contraseña',
              labelText: 'Contraseña',
            ),
          ),
        );
    }

    widgetCorreo() {
        return  Container(
          child: TextField(
            controller: correo,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              icon: const Icon(Icons.mail),
              hintText: 'Correo electronico',
              labelText: 'Correo electronico',
            ),
          ),
        );
    }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children:
               [
                Padding(
                  padding: const EdgeInsets.fromLTRB(5, 30, 5, 15),
                  child: Text("Datos para registro:",
                  style: TextStyle(fontSize: 20),  
                  ),
                ),

                widgetUsername(),
                widgetCorreo(),
                widgetPassword(),
                ElevatedButton(
                  onPressed: (){
                   
                    validarRegistro();
                  }, 
                  child:Text("Registrar"),
                  )
              ],
            ),
          ),
        ),
      ),
    );
    
  }
  bool camposValidos() {
    if(nombre.text.toString().isEmpty || clave.text.toString().isEmpty || correo.text.toString().isEmpty)
      {return false;}
      else{
       return true;
      }
   
  }

}