import 'dart:async';

import 'package:paquete_mongo/paquete_mongo.dart';
import 'package:ohanami/repositorio_local/repositorio_local.dart';
import 'componentes.dart';
import 'package:flutter/material.dart';

class SelectorVista extends StatefulWidget {
  const SelectorVista({ Key? key,}) : super(key: key);

  @override
  State<SelectorVista> createState() => _SelectorVistaState();
}

class _SelectorVistaState extends State<SelectorVista> {
  bool usuarioRegistrado = false;
  TextEditingController nombre = TextEditingController();
  TextEditingController correo = TextEditingController();
  TextEditingController clave = TextEditingController();
  @override
    void initState() {

      validacion().whenComplete(() async {    
        usuarioRegistrado == false ? 
        Navigator.push( context, MaterialPageRoute( builder: (context) => VistaIniciarSesion())) :
        Navigator.push( context, MaterialPageRoute(builder: (context) => VistaListaPartidas()));
      });
    super.initState();
  }

  Future validacion() async{
    RepositorioMongo mongo = RepositorioMongo();
    RepositorioLocal local = RepositorioLocal();
    //local.eliminarUsuario();
    bool mongocheck = await mongo.inicializar();
    bool localcheck = await local.usuarioYaRegistrado();

    if (mongocheck == true && localcheck == true) {
      //Usuario usuario = await local.recuperarUsuarioLocal();
      setState(() {
        usuarioRegistrado = true;
      });
    }
    if (mongocheck == false && localcheck == true) {
      setState(() {
        usuarioRegistrado = localcheck;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [const CircularProgressIndicator(
              )
            ],
          ),
        ),
      ),
    );
  }
}