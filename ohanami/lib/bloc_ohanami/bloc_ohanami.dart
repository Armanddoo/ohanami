import 'package:ohanami/bloc_ohanami/estados.dart';
import 'package:ohanami/bloc_ohanami/eventos.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:partida/partida.dart';

class OhanamiBloc extends Bloc<Evento, Estados_ohanami> {
  OhanamiBloc(this.partida) : super(Ronda1(partida)) {
    on<SiguienteRonda1>(_onRonda1);
    on<SiguienteRonda2>(_onRonda2);
    on<SiguienteRonda3>(_onRonda3);
  }
  
  Partida partida;

  void _onRonda1 (SiguienteRonda1 evento, Emitter<Estados_ohanami> emit){
    partida = evento.partida;

    emit(Ronda1(partida));
  }
  void _onRonda2 (SiguienteRonda2 evento, Emitter<Estados_ohanami> emit){
    partida = evento.partida;
    emit(Ronda2(partida));
  }
  
  void _onRonda3 (SiguienteRonda3 evento, Emitter<Estados_ohanami> emit){
    partida = evento.partida;

    emit(Ronda3(partida));
  }

  
}
